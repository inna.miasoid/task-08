package com.epam.rd.java.basic.task8.model;

public class GrowingTips {
    private int tempreture;
    private String tempretureAttributeMeasure;
    private String lightRequiring;
    private int watering;
    private String wateringAttributeMeasure;

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public void setTempretureAttributeMeasure(String tempretureAttributeMeasure) {
        this.tempretureAttributeMeasure = tempretureAttributeMeasure;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public void setWateringAttributeMeasure(String wateringAttributeMeasure) {
        this.wateringAttributeMeasure = wateringAttributeMeasure;
    }

    public int getTempreture() {
        return tempreture;
    }

    public String getTempretureAttributeMeasure() {
        return tempretureAttributeMeasure;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    public int getWatering() {
        return watering;
    }

    public String getWateringAttributeMeasure() {
        return wateringAttributeMeasure;
    }

    @Override
    public String toString() {
        return "{" +
                "tempreture=" + tempreture + " " +
                 tempretureAttributeMeasure + '\'' +
                ", lightRequiring='" + lightRequiring + '\'' +
                ", watering=" + watering + " " +
                 wateringAttributeMeasure + '\'' +
                '}';
    }
}
