package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.GrowingTips;
import com.epam.rd.java.basic.task8.model.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class MapObjectHandlerSax extends DefaultHandler {
    private StringBuilder currentValue = new StringBuilder();
    private List<Flower> flowers;
    private Flower currentFlower;
    private VisualParameters currentFlowerVisualParameters;
    private GrowingTips currentFlowerGrowingTips;

    public List<Flower> getFlowers() {
        return flowers;
    }

    @Override
    public void startDocument() {
        flowers = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {

        currentValue.setLength(0);
        if (qName.equalsIgnoreCase("flower")) {
            currentFlower = new Flower();
            currentFlowerVisualParameters = new VisualParameters();
            currentFlowerGrowingTips = new GrowingTips();
        }

        if (qName.equalsIgnoreCase("aveLenFlower")) {
            currentFlowerVisualParameters.setAveLenFlowerAttributeMeasure(attributes.getValue("measure"));
        }

        if (qName.equalsIgnoreCase("tempreture")) {
            currentFlowerGrowingTips.setTempretureAttributeMeasure(attributes.getValue("measure"));
        }

        if (qName.equalsIgnoreCase("watering")) {
            currentFlowerGrowingTips.setWateringAttributeMeasure(attributes.getValue("measure"));
        }

        if (qName.equalsIgnoreCase("lighting")) {
            String lightRequiring = attributes.getValue("lightRequiring");
            currentFlowerGrowingTips.setLightRequiring(lightRequiring);
        }
    }

    public void endElement(String uri, String localName, String qName) {

        if (qName.equalsIgnoreCase("name")) {
            currentFlower.setName(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("soil")) {
            currentFlower.setSoil(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("origin")) {
            currentFlower.setOrigin(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("multiplying")) {
            currentFlower.setMultiplying(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("stemColour")) {
            currentFlowerVisualParameters.setStemColour(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("leafColour")) {
            currentFlowerVisualParameters.setLeafColour(currentValue.toString());
        }

        if (qName.equalsIgnoreCase("aveLenFlower")) {
            currentFlowerVisualParameters.setAveLenFlower(Integer.parseInt(String.valueOf(currentValue)));
        }
        currentFlower.setVisualParameters(currentFlowerVisualParameters);

        if (qName.equalsIgnoreCase("tempreture")) {
            currentFlowerGrowingTips.setTempreture(Integer.parseInt(String.valueOf(currentValue)));
        }

        if (qName.equalsIgnoreCase("watering")) {
            currentFlowerGrowingTips.setWatering(Integer.parseInt(String.valueOf(currentValue)));
        }
        currentFlower.setGrowingTips(currentFlowerGrowingTips);

        if (qName.equalsIgnoreCase("flower")) {
            flowers.add(currentFlower);
        }
    }

    public void characters(char[] ch, int start, int length) {
        currentValue.append(ch, start, length);
    }
}
