package com.epam.rd.java.basic.task8.comparator;

import com.epam.rd.java.basic.task8.model.Flower;

import java.util.Comparator;

public class SortByWateringComparator implements Comparator<Flower> {
    @Override
    public int compare(Flower o1, Flower o2) {
        int firstFlowerWatering = o1.getGrowingTips().getWatering();
        int secondFlowerWatering = o2.getGrowingTips().getWatering();
        return Integer.toString(firstFlowerWatering).compareTo(Integer.toString(secondFlowerWatering));
    }
}
