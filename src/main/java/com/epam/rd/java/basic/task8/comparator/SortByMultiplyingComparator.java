package com.epam.rd.java.basic.task8.comparator;

import com.epam.rd.java.basic.task8.model.Flower;

import java.util.Comparator;

public class SortByMultiplyingComparator implements Comparator<Flower> {
    @Override
    public int compare(Flower o1, Flower o2) {
        return o1.getMultiplying().compareTo(o2.getMultiplying());
    }
}
