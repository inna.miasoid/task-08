package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.comparator.SortByMultiplyingComparator;
import com.epam.rd.java.basic.task8.comparator.SortByNameComparator;
import com.epam.rd.java.basic.task8.comparator.SortByWateringComparator;
import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.convertor.Convertor;
import com.epam.rd.java.basic.task8.model.Flower;

import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		List<Flower> domFlowers = domController.getContainer();

		// sort (case 1)
		domFlowers.sort(new SortByNameComparator());

		// save
		Convertor convertor = new Convertor("output.dom.xml");
		convertor.createXMLOutput(domFlowers);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		List<Flower> saxFlowers = saxController.getContainer();

		// sort  (case 2)
		saxFlowers.sort(new SortByMultiplyingComparator());
		
		// save
		Convertor saxConvertor = new Convertor("output.sax.xml");

		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
//		STAXController staxController = new STAXController(xmlFileName);
		SAXController staxController = new SAXController(xmlFileName);
		List<Flower> staxFlowers = staxController.getContainer();
		
		// sort  (case 3)

		saxFlowers.sort(new SortByWateringComparator());
		System.out.println(staxFlowers);;
		// save
		Convertor staxConvertor = new Convertor("output.stax.xml");
		staxConvertor.createXMLOutput(saxFlowers);
	}
}
