package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.GrowingTips;
import com.epam.rd.java.basic.task8.model.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> getContainer() throws ParserConfigurationException, IOException, SAXException {
		List<Flower> flowers = new ArrayList<>();
		File file = new File(xmlFileName);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document doc = dbf.newDocumentBuilder().parse(file);

		NodeList flowersList = doc.getElementsByTagName("flower");

		for (int i = 0; i < flowersList.getLength(); i++) {
			Node node = flowersList.item(i);
			Flower flower = new Flower();
			VisualParameters visualParameters = new VisualParameters();
			GrowingTips growingTips = new GrowingTips();

			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;

				String name = element.getElementsByTagName("name").item(0).getTextContent();
				flower.setName(name);

				String soil = element.getElementsByTagName("soil").item(0).getTextContent();
				flower.setSoil(soil);

				String origin = element.getElementsByTagName("origin").item(0).getTextContent();
				flower.setOrigin(origin);

				String multiplying = element.getElementsByTagName("multiplying").item(0).getTextContent();
				flower.setMultiplying(multiplying);
			}

			NodeList visualNodeList = doc.getElementsByTagName("visualParameters");
			Node visualNode = visualNodeList.item(i);

			if (visualNode.getNodeType() == Node.ELEMENT_NODE) {
				Element visualElement = (Element) node;

				String stemColour = visualElement.getElementsByTagName("stemColour").item(0).getTextContent();
				visualParameters.setStemColour(stemColour);

				String leafColour = visualElement.getElementsByTagName("leafColour").item(0).getTextContent();
				visualParameters.setLeafColour(leafColour);

				NodeList aveLenFlowerNodeList = visualElement.getElementsByTagName("aveLenFlower");
				int aveLenFlower = Integer.parseInt(aveLenFlowerNodeList.item(0).getTextContent());
				visualParameters.setAveLenFlower(aveLenFlower);

				String aveLenFlowerAttributeMeasure = aveLenFlowerNodeList.item(0).getAttributes().getNamedItem("measure").getTextContent();
				visualParameters.setAveLenFlowerAttributeMeasure(aveLenFlowerAttributeMeasure);
			}

			NodeList growingNodeList = doc.getElementsByTagName("growingTips");
			Node growingNode = growingNodeList.item(i);

			if (growingNode.getNodeType() == Node.ELEMENT_NODE) {
				Element growingElement = (Element) node;

				NodeList lightingNodeList = growingElement.getElementsByTagName("lighting");
				String lightRequiring = lightingNodeList.item(0).getAttributes().getNamedItem("lightRequiring").getTextContent();
				growingTips.setLightRequiring(lightRequiring);

				NodeList temperatureNodeList = growingElement.getElementsByTagName("tempreture");
				int temperature = Integer.parseInt(temperatureNodeList.item(0).getTextContent());
				growingTips.setTempreture(temperature);

				String tempretureAttributeMeasure = temperatureNodeList.item(0).getAttributes().getNamedItem("measure").getTextContent();
				growingTips.setTempretureAttributeMeasure(tempretureAttributeMeasure);

				NodeList wateringNodeList = growingElement.getElementsByTagName("watering");
				int watering = Integer.parseInt(wateringNodeList.item(0).getTextContent());
				growingTips.setWatering(watering);

				String wateringAttributeMeasure = wateringNodeList.item(0).getAttributes().getNamedItem("measure").getTextContent();
				growingTips.setWateringAttributeMeasure(wateringAttributeMeasure);
			}
			flower.setGrowingTips(growingTips);
			flower.setVisualParameters(visualParameters);
			flowers.add(flower);
		}
		return flowers;
	}
}
