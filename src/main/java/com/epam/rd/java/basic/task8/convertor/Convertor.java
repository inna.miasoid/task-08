package com.epam.rd.java.basic.task8.convertor;

import com.epam.rd.java.basic.task8.model.Flower;
import com.epam.rd.java.basic.task8.model.GrowingTips;
import com.epam.rd.java.basic.task8.model.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class Convertor {
    private String outputXMLFileName;

    public Convertor(String outputXMLFileName) {
        this.outputXMLFileName = outputXMLFileName;
    }

    public void createXMLOutput(List<Flower> flowers) throws ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("flowers");
        rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xmlns", "http://www.nure.ua");
        rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
        doc.appendChild(rootElement);

        for (Flower flower : flowers) {
            VisualParameters visualParameters = flower.getVisualParameters();
            GrowingTips growingTips = flower.getGrowingTips();

            Element flowerElement = doc.createElement("flower");
            rootElement.appendChild(flowerElement);

            Element name = doc.createElement("name");
            name.setTextContent(flower.getName());
            flowerElement.appendChild(name);

            Element soil = doc.createElement("soil");
            soil.setTextContent(flower.getSoil());
            flowerElement.appendChild(soil);

            Element origin = doc.createElement("origin");
            origin.setTextContent(flower.getOrigin());
            flowerElement.appendChild(origin);

            Element visualPar = doc.createElement("visualParameters");
            flowerElement.appendChild(visualPar);

            Element stemColour = doc.createElement("stemColour");
            stemColour.setTextContent(visualParameters.getStemColour());
            visualPar.appendChild(stemColour);

            Element leafColour = doc.createElement("leafColour");
            leafColour.setTextContent(visualParameters.getLeafColour());
            visualPar.appendChild(leafColour);

            Element aveLenFlower = doc.createElement("aveLenFlower");
            aveLenFlower.setAttribute("measure", visualParameters.getAveLenFlowerAttributeMeasure());
            aveLenFlower.setTextContent(Integer.toString(visualParameters.getAveLenFlower()));
            visualPar.appendChild(aveLenFlower);

            Element growTips = doc.createElement("growingTips");
            flowerElement.appendChild(growTips);

            Element tempreture = doc.createElement("tempreture");
            tempreture.setAttribute("measure", growingTips.getTempretureAttributeMeasure());
            tempreture.setTextContent(Integer.toString(growingTips.getTempreture()));
            growTips.appendChild(tempreture);


            Element lighting = doc.createElement("lighting");
            lighting.setAttribute("lightRequiring", growingTips.getLightRequiring());
            growTips.appendChild(lighting);

            Element watering = doc.createElement("watering");
            watering.setAttribute("measure", growingTips.getWateringAttributeMeasure());
            watering.setTextContent(Integer.toString(growingTips.getWatering()));
            growTips.appendChild(watering);

            Element multiplying = doc.createElement("multiplying");
            multiplying.setTextContent(flower.getMultiplying());
            flowerElement.appendChild(multiplying);
        }

        try (FileOutputStream output = new FileOutputStream(outputXMLFileName)) {
            writeXml(doc, output);
        } catch (IOException | TransformerException e) {
            e.printStackTrace();
        }
    }

    private static void writeXml(Document doc, OutputStream output) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(output);
        transformer.transform(source, result);
    }
}
