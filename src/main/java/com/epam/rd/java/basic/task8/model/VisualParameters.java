package com.epam.rd.java.basic.task8.model;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private int aveLenFlower;
    private String aveLenFlowerAttributeMeasure;

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public void setAveLenFlowerAttributeMeasure(String aveLenFlowerAttributeMeasure) {
        this.aveLenFlowerAttributeMeasure = aveLenFlowerAttributeMeasure;
    }

    public String getAveLenFlowerAttributeMeasure() {
        return aveLenFlowerAttributeMeasure;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    @Override
    public String toString() {
        return "{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower + " " +
                aveLenFlowerAttributeMeasure + '\'' +
                '}';
    }
}
