package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ListResourceBundle;

/**
 * Controller for SAX parser.
 */
public class SAXController {

	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> getContainer() {
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		List<Flower> flowers = new ArrayList<>();

		try (InputStream is = getXMLFileAsStream()) {
			SAXParser saxParser = saxParserFactory.newSAXParser();
			MapObjectHandlerSax handler = new MapObjectHandlerSax();
			saxParser.parse(is, handler);
			flowers = handler.getFlowers();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return flowers;
	}

	private InputStream getXMLFileAsStream() throws FileNotFoundException {
		return new BufferedInputStream(new FileInputStream(xmlFileName));
	}
}